import React from "react";
import {Component} from "react";
import { AppRegistry, YellowBox} from 'react-native';
import { MainView } from './src/main/MainView';
import { JobInfo } from './src/jobInfo/jobinfo';
import { createStackNavigator } from 'react-navigation';
import { store } from './src/store';
import { Provider } from 'react-redux';
import {loginView} from "./src/login/LoginView";
import { MainViewMapped } from "./src/main/MainView";


class AppWithStore extends Component {
    render (){
        return(
            <Provider store = {store} >
                <App/>
            </Provider>
        );
    }
}


  const App = createStackNavigator(
    {
        LoginScreen: loginView,
        MainScreen: MainViewMapped,
        JobInfoScreen: JobInfo,

    },
    {
        initialRouteName: "LoginScreen",
        headerMode: 'none',
    });

AppRegistry.registerComponent('testProject', () => AppWithStore);
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Warning: Each child in an array'])
