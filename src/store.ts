import {AnyAction, applyMiddleware, createStore} from "redux";
import thunk, {ThunkMiddleware} from "redux-thunk";
import { reducer } from "./reducers/rootReducer";

export interface IState {
    areAuthorized: boolean;
    services: any[];
    schedule: any[];
    activeTime: string;
    activeDay: number;
    index: number;
    time: Date;
}

export const initialState: IState = {
    areAuthorized: false,
    services: [],
    schedule: [],
    activeTime: "",
    activeDay: 0,
    index: 0,
    time: new Date(Date.now())
};

export const store = createStore(reducer, applyMiddleware(thunk as ThunkMiddleware<IState, AnyAction>));
