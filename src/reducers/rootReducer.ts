import {initialState, IState} from "../store";
import {login} from "../actions/login";
import {isType} from "typescript-fsa";
import {addServices} from "../actions/getServices";
import {changeActiveDay, changeActiveTime, changeIndex, getSchedule} from "../actions/jobInfo";

//export const reducer = combineReducers({LoginReducer});

export const reducer = function (state: IState = initialState, action: any): IState {
    if (isType(action, login)) {
        return {...state, areAuthorized: true};
    }

    if (isType(action, addServices)) {
        return {...state, services: action.payload};
    }

    if (isType(action, changeIndex)) {
        return {...state, index: action.payload};
    }

    if (isType(action, getSchedule)) {
        return {...state, schedule: action.payload};
    }

    if (isType(action, changeActiveTime)) {
        return {...state, activeTime: action.payload};
    }

    if (isType(action, changeActiveDay)) {
        return {...state, activeDay: action.payload};
    }

    return state;
};