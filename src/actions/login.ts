import {actionCreatorFactory} from "typescript-fsa";
import {LOGIN} from "./actionTypes";
import {asyncFactory} from "typescript-fsa-redux-thunk";

const actionCreator = actionCreatorFactory();

const asyncActionCreator = asyncFactory<any>(actionCreator);

export const login = actionCreator<void>(LOGIN);

export interface ILoginParams {
    login: string;
    password: string;
}

export const loginRequest = asyncActionCreator<ILoginParams, void>("Login", async (params, dispatch) => {
    const url = "http://176.31.32.73:8001/api/User/Login";
    const request = {
        body: JSON.stringify(params),
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        method: "POST",
    };
    const response = await fetch(url, request);
    if (response.ok) {
        dispatch(login());
    }
});