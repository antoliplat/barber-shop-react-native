import actionCreatorFactory from "typescript-fsa";
import {ADD_SCHEDULE, CHANGE_ACTIVE_DAY, CHANGE_ACTIVE_TIME, CHANGE_INDEX} from "./actionTypes";
import {asyncFactory} from "typescript-fsa-redux-thunk";

const actionCreator = actionCreatorFactory();
const asyncActionCreator = asyncFactory<any>(actionCreator);

export const changeIndex = actionCreator<number>(CHANGE_INDEX);

export const getSchedule = actionCreator<any[]>(ADD_SCHEDULE);

export const changeActiveTime = actionCreator<string>(CHANGE_ACTIVE_TIME);

export const changeActiveDay = actionCreator<number>(CHANGE_ACTIVE_DAY);

export const ScheduleRequest = asyncActionCreator<any, void>("GetSchedule", async (params, dispatch) => {
    const url = "http://176.31.32.73:8001/api/Job/GetSchedule";
    const request = {
        body: JSON.stringify(params),
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        method: "POST",
    };
    const response = await fetch(url, request);
    if (response.ok) {
        const schedule = await response.json();
        dispatch(getSchedule(schedule.times));
    }
});