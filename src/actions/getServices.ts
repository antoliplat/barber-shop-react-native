import actionCreatorFactory from "typescript-fsa";
import {asyncFactory} from "typescript-fsa-redux-thunk";
import {ADD_SERVICES} from "./actionTypes";

const actionCreator = actionCreatorFactory();

const asyncActionCreator = asyncFactory<any>(actionCreator);

export const addServices = actionCreator<any[]>(ADD_SERVICES);

export const serviceRequest = asyncActionCreator<void, void>("GetServices", async (params, dispatch) => {
    const url = "http://176.31.32.73:8001/api/Job/GetAll";
    const request = {
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
        },
        method: "GET",
    };
    const response = await fetch(url, request);
    if (response.ok) {
        const json = await response.json();
        dispatch(addServices(json));
    }
});