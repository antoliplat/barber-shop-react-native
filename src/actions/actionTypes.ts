export const LOGIN = "LOGIN";

export  const ADD_SERVICES = "ADD_SERVICES";

export const ADD_SCHEDULE = "ADD_SCHEDULE";

export const CHANGE_ACTIVE_TIME = "CHANGE_ACTIVE_TIME";

export const CHANGE_ACTIVE_DAY = "CHANGE_ACTIVE_DAY";

export const CHANGE_INDEX = "CHANGE_INDEX";