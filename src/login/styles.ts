import {
    Platform,
    StyleSheet,
} from "react-native";

export const styles = StyleSheet.create({
    background: {
        flex: 1,
    },
    bottomBar: {
        flexDirection: "row",
        justifyContent: "center",
    },
    button: {
        backgroundColor: "#B09B7A",
        borderRadius: 2,
    },
    buttonText: {
        color: "#FFFFFF",
        fontFamily: Platform.OS === "ios" ? "SFUIText-Regular" : "",
        fontSize: 17,
        textAlign: "center",
    },
    container: {
        alignItems: "stretch",
        flex: 0.7,
        justifyContent: "space-between",
        marginLeft: 64,
        marginRight: 64,
    },
    gradient: {
        flex: 1,
        justifyContent: "center",
    },
    inputLogin: {
        color: "#FFFFFF",
    },
    inputPassword: {
        color: "#FFFFFF",
    },
    logo: {
        height: 66,
        resizeMode: "center",
    },
    networks: {
        height: 50,
        marginHorizontal: 10,
        width: 50,
    },
    registretionLabel: {
        color: "#FFFFFF",
        fontFamily: Platform.OS === "ios" ? "SFUIText-Regular" : "",
        textAlign: "right",
    },
    widthContainer: {
        alignItems: "stretch",
    },
});
