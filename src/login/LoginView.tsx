import React from "react";
import {Component} from "react";
import {
    Button,
    Image,
    ImageBackground,
    Keyboard,
    Text,
    TextInput,
    ToastAndroid,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import {constants} from "../constants/text";
import {styles} from "./styles";
import {connect} from "react-redux";
import {loginRequest} from "../actions/login";
import {store} from "../store";
import {serviceRequest} from "../actions/getServices";

interface IState {
    login: string;
    password: string;
}

export class Login extends Component<any, IState> {
    constructor(props: any) {
        super(props);
        this.state = {
            login: "",
            password: ""
        };
    }

    unsubscribe = store.subscribe(() => {
        if (store.getState().areAuthorized) {
            this.unsubscribe();
            this.props.onAuthorize();
            this.props.navigation.replace("MainScreen");
        }
    });

    onLoginPress(): void {
        this.props.onLoginPress({login: this.state.login, password: this.state.password});
    }

    onRegistrationPress(): void {
        // Чтобы делало хоть что-то
        ToastAndroid.show("Not implemented", ToastAndroid.SHORT);
    }

    onNetworkPress(): void {
        // Возможно немного позже добавлю реализацию
        ToastAndroid.show("Not implemented", ToastAndroid.SHORT);
    }

    onLoginTextChange(text: string): void {
        this.setState({...this.state, login: text});
    }

    onPasswordTextChange(text: string): void {
        this.setState({...this.state, password: text});
    }

    onBackgroundPress(): void {
        Keyboard.dismiss();
    }

    public render(): JSX.Element {
        return (
            <ImageBackground source = {require("../../assets/Image/barbershop.png")} style = {styles.background}>
                <TouchableWithoutFeedback
                    style = {styles.background}
                    onPress = {this.onBackgroundPress}
                >
                    <LinearGradient colors={["#464646A0", "#46464681"]} style = {styles.gradient}>
                        <View style = {styles.container}>
                            <Image source={{uri: "logo"}} style = {styles.logo}/>
                            <View style = {styles.widthContainer}>
                                <TextInput
                                    placeholder = {constants.EMAIL}
                                    onChangeText = {this.onLoginTextChange.bind(this)}
                                    style = {styles.inputLogin}
                                    placeholderTextColor = "#A5A5A5"
                                    underlineColorAndroid = "#A5A5A5"
                                    keyboardType = "email-address"
                                />
                                <TextInput
                                    placeholder = {constants.PASSWORD}
                                    onChangeText = {this.onPasswordTextChange.bind(this)}
                                    style = {styles.inputPassword}
                                    placeholderTextColor = "#A5A5A5"
                                    underlineColorAndroid = "#A5A5A5"
                                    secureTextEntry={true}
                                />
                                <Button
                                    onPress = {this.onLoginPress.bind(this)}
                                    title = {constants.ENTRY}
                                    color = "#B09B7A"
                                />
                                <Text style = {styles.registretionLabel} onPress = {this.onRegistrationPress}>
                                    {constants.REGISTRATION}
                                </Text>
                            </View>
                            <View style = {styles.bottomBar}>
                                <TouchableOpacity onPress={this.onNetworkPress}>
                                    <Image source = {{uri: "icon_twitter"}} style = {styles.networks}/>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.onNetworkPress}>
                                    <Image source = {{uri: "icon_facebook"}} style = {styles.networks} />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={this.onNetworkPress}>
                                    <Image source = {{uri: "icon_vk"}} style = {styles.networks} />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </LinearGradient>
                </TouchableWithoutFeedback>
            </ImageBackground>
        );
    }
}

const mapDispatchToProps = (dispatch: any): any => {
    return {
        onLoginPress: (loginParams: any): void => {
            dispatch(loginRequest.action(loginParams));
        },
        onAuthorize: (): void => {
            dispatch(serviceRequest.action());
        },
    };
};

export const loginView = connect(null, mapDispatchToProps)(Login);