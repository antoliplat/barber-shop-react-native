import React from "react";
import {Component} from "react";
import {
    ImageBackground,
    Text,
    View,
    Image,
    TouchableOpacity,
    ToastAndroid,
} from "react-native";
import {styles} from "./styles";
import LinearGradient from "react-native-linear-gradient";
import { constants } from "../../constants/text";

export class StaticPart extends Component<any, any> {

    onMenuPress(): void {
        ToastAndroid.show("Not implemented", ToastAndroid.SHORT);
    }

    public render(): JSX.Element {
        return (
            <ImageBackground
                source = {require("../../../assets/Image/barbershop.png")}
                style = {styles.background}
            >
                <LinearGradient
                    start = {{x: 0, y: 0.5}}
                    end = {{x: 0.5, y: 0}}
                    colors={["#464646FF", "#464646E1", "#46464699"]}
                    style = {styles.gradient}
                >
                    <View style = {styles.topContainer}>
                        <View style = {styles.topBar}>
                            <TouchableOpacity onPress = {this.onMenuPress}>
                                <Image source = {{uri: "icon_menu"}} style = {styles.menuIcon}/>
                            </TouchableOpacity>
                            <View style = {styles.rightPadding} >
                                <Image source = {{uri: "logo"}} style = {styles.logo}/>
                            </View>
                        </View>
                        <View style = {styles.bottomText}>
                            <Text style = {styles.salon}>
                                {constants.SALON}</Text>
                            <Text style = {styles.name}>
                                {constants.NAME}</Text>
                            <Text style = {styles.adress}>
                                {constants.ADRESS}</Text>
                        </View>
                    </View>
                </LinearGradient>
            </ImageBackground>
        );
    }
}