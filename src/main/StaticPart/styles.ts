import {Platform, StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    background: {
        flex: 1,
        // borderBottomColor: "#E5C999",
        // borderBottomWidth: 1,
    },
    gradient: {
        flex: 1,
    },
    menuIcon: {
        width: 50,
        height: 50,
        resizeMode: "center",
    },
    logo: {
        resizeMode: "center",
        width: 110,
        height: 50,
        marginLeft: "auto",
        marginRight: "auto",
    },
    topBar: {
        flex: 0.3,
        flexDirection: "row",
    },
    name: {
        fontSize: 28,
        fontFamily: "Pattaya-Regular",
        color: "#FFFFFFFF",
        paddingLeft: 16,
    },
    adress: {
        fontFamily: "KellySlab-Regular",
        fontSize: 22,
        color: "#FFFFFFFF",
        paddingLeft: 16,
    },
    salon: {
        color: "#E5C999FF",
        fontSize: 22,
        paddingLeft: 16,
        fontFamily: Platform.OS === "ios" ? "SFUIText-Regular" : "",
    },
    topContainer: {
        flex: 1,
        justifyContent: "space-between",
    },
    bottomContainer: {
        backgroundColor: "#464646FF",
        flex: 0.5,
    },
    rightPadding: {
        flex: 1,
        paddingRight: 50,
    },
    bottomText: {
        paddingBottom: 12
    }
});