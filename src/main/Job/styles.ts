import {Platform, StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    itemBackground: {
        height: 184,
    },
    backround: {
        backgroundColor: "#464646",
        paddingHorizontal: 16,
        paddingVertical: 8,
    },
    firstGradient: {
        flex: 1,
        justifyContent: "flex-end",
    },
    secondGradient: {
        flex: 0.6,
    },
    itemTitle: {
        flexDirection: "row",
        justifyContent: "space-between",
        borderBottomColor: "#E5C999",
        borderBottomWidth: 1,
    },
    name: {
        color: "#FFFFFF",
        fontSize: 22,
        paddingHorizontal: 8,
        paddingVertical: 6,
        fontFamily: Platform.OS === "ios" ? "SFUIText-Regular" : "",
    },
    price: {
        color: "#FFFFFF",
        fontSize: 22,
        paddingHorizontal: 8,
        paddingVertical: 6,
        fontFamily: Platform.OS === "ios" ? "SFUIText-Regular" : "",
    },
    bottomPart: {
        flexDirection: "row",
        justifyContent: "space-between",
        paddingHorizontal: 8,
        paddingVertical: 6,
    },
    description: {
        fontSize: 12,
        color: "#FFFFFF",
        width: 215,
        fontFamily: Platform.OS === "ios" ? "SFUIText-Light" : "",
    },
    duration: {
        fontSize: 12,
        color: "#FFFFFF",
        fontFamily: Platform.OS === "ios" ? "SFUIText-Regular" : "",
    }
});