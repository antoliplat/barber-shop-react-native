import React from "react";
import {Component} from "react";
import {
    ImageBackground,
    View,
    Text,
    TouchableHighlight,
} from "react-native";
import {styles} from "./styles";
import LinearGradient from "react-native-linear-gradient";
import { constants } from "../../constants/text";

export class Job extends Component<any, any> {

    onItemPress(): void {
        this.props.onItemPress(this.props.data.index);
    }

    getDuration(): string {
        return this.props.data.item.duration.replace("00:", "").replace(":00", "") + " " + constants.MINUTES_SHORT;
    }

    public render(): JSX.Element {
        return (
            <View style = {styles.backround}>
                <TouchableHighlight onPress = {this.onItemPress.bind(this)}>
                    <ImageBackground
                        source = {{uri: this.props.data.item.imageUrl}}
                        style = {styles.itemBackground}
                    >
                        <LinearGradient
                            colors = {["#A2A2A200", "#464646FF"]}
                            style = {styles.firstGradient}
                        >
                            <LinearGradient
                                style = {styles.secondGradient}
                                colors = {["#74695466", "#3232322D"]}
                            >
                                <View style = {styles.itemTitle}>
                                    <Text style = {styles.name}>
                                        {this.props.data.item.name}
                                    </Text>
                                    <Text style = {styles.price}>
                                        {this.props.data.item.price}
                                    </Text>
                                </View>
                                <View style = {styles.bottomPart}>
                                    <Text style = {styles.description}>
                                        {this.props.data.item.description}
                                    </Text>
                                    <Text style = {styles.duration}>
                                        {this.getDuration()}
                                    </Text>
                                </View>
                            </LinearGradient>
                        </LinearGradient>
                    </ImageBackground>
                </TouchableHighlight>
            </View>
        );
    }
}