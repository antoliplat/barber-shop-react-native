import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    listTitle: {
        color: "#E5C999",
        fontFamily: "KellySlab-Regular",
        fontSize: 20,
        textAlign: "center",
        backgroundColor: "#464646",
        paddingTop: 16,
    },
    container: {
        flex: 1,
        backgroundColor: "#464646",
    },
    border: {
        backgroundColor: "#E5C999",
        marginLeft: "25%",
        height: 1.5,
    }
});