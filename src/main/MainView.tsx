import React, { ReactElement } from "react";
import {Component} from "react";
import {
    View,
    FlatList,
    Text,
} from "react-native";
import { StaticPart } from "./StaticPart/StaticPart";
import { Job } from "./Job/Job";
import {IState} from "../store";
import { withNavigation } from "react-navigation";
import { constants } from "../constants/text";
import { styles } from "./styles";
import {connect} from "react-redux";

export class Main extends Component<any, any> {

    onItemPress(index: number): void {
        this.props.navigation.push("JobInfoScreen", {jobId: index});
    }

    addKeys(item: any, index: number): any {
        return {...item, key: index.toString()};
    }

    getJobItem(item: any): ReactElement<any> {
        return (
            <Job
                data = {item}
                onItemPress = {this.onItemPress.bind(this)}
            />
        );
    }

    listTitle(): ReactElement<any> {
        return (
            <Text style = {styles.listTitle}>
                {constants.JOBS_OF_SALON}
            </Text>
        );
    }

    public render(): JSX.Element {
        return (
            <View style = {styles.container}>
                <StaticPart/>
                <View style = {styles.border}/>
                <FlatList
                    data = {this.props.services.map(this.addKeys)}
                    renderItem = {this.getJobItem.bind(this)}
                    ListHeaderComponent = {this.listTitle}
                />
            </View>
        );
    }
}

export const MainView = withNavigation(Main);

const mapStateToProps = (state: IState): any => {
    return {
        services: state.services,
    };
};

export const MainViewMapped = connect(mapStateToProps)(MainView);