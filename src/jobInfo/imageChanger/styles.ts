import {Dimensions, StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    images: {
        flexDirection: "row",
        paddingVertical: 16,
        alignSelf: "center",
        width: "100%",
        height: Dimensions.get("screen").width * 0.4 + 20,
    },
    leftImage: {
        width: Dimensions.get("screen").width * 0.4,
        height: Dimensions.get("screen").width * 0.4,
        resizeMode: "cover",
        borderRadius: 180,
    },
    centerImage: {
        width: Dimensions.get("screen").width * 0.4,
        height: Dimensions.get("screen").width * 0.4,
        resizeMode: "cover",
        borderRadius: 180,
        borderWidth: 5,
        borderColor: "#E5C999",
    },
    rightImage: {
        width: Dimensions.get("screen").width * 0.4,
        height: Dimensions.get("screen").width * 0.4,
        resizeMode: "cover",
        borderRadius: 180,
    },
    emptyView: {
        width: 170,
        height: 170,
    },
    leftImageContainer: {
        position: "absolute",
        left: -30,
        top: 10,
    },
    centerImageContainer: {
        position: "absolute",
        top: 10,
        left: (Dimensions.get("screen").width * 0.6) / 2,
        zIndex: 10,
    },
    rightImageContainer: {
        position: "absolute",
        right: -30,
        top: 10,
    }
});