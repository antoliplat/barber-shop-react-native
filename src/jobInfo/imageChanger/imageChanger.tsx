import React from "react";
import { Component } from "react";
import {
    Image,
    TouchableOpacity,
    View,
} from "react-native";
import { styles } from "./styles";

export class ImageChanger extends Component<any, any> {

    onRightPress(): void {
        this.props.onImagePress(this.props.index + 1);
    }

    onLeftPress(): void {
        this.props.onImagePress(this.props.index - 1);
    }

    getImageUrl(position: number): string {
        return this.props.services[position].imageUrl;
    }

    render(): JSX.Element {
        switch (this.props.index) {
            case 0: {
                return (
                    <View style = {styles.images}>
                        <View style = {styles.leftImageContainer} >
                            <View style = {styles.emptyView} />
                        </View>
                        <View style = {styles.centerImageContainer}>
                            <Image
                                source = {{uri: this.getImageUrl(this.props.index)}}
                                style = {styles.centerImage}
                            />
                        </View>
                        <TouchableOpacity onPress = {this.onRightPress.bind(this)} style = {styles.rightImageContainer}>
                            <Image
                                source = {{uri: this.getImageUrl(this.props.index + 1)}}
                                style = {styles.rightImage}
                            />
                        </TouchableOpacity>
                    </View>
                );
            }
            case this.props.services.length - 1: {
                return (
                    <View style = {styles.images}>
                        <TouchableOpacity onPress = {this.onLeftPress.bind(this)} style = {styles.leftImageContainer}>
                            <Image
                                source = {{uri: this.getImageUrl(this.props.index - 1) }}
                                style = {styles.leftImage}
                            />
                        </TouchableOpacity>
                        <View style = {styles.centerImageContainer}>
                            <Image
                                source = {{uri: this.getImageUrl(this.props.index)}}
                                style = {styles.centerImage}
                            />
                        </View>
                        <View style = {styles.rightImageContainer}>
                            <View style = {styles.emptyView}/>
                        </View>
                    </View>
                );
            }
            default: {
                return (
                    <View style = {styles.images}>
                        <TouchableOpacity onPress = {this.onLeftPress.bind(this)} style = {styles.leftImageContainer}>
                            <Image
                                source = {{uri: this.getImageUrl(this.props.index - 1) }}
                                style = {styles.leftImage}
                            />
                        </TouchableOpacity>
                        <View style = {styles.centerImageContainer}>
                            <Image
                                source = {{uri: this.getImageUrl(this.props.index)}}
                                style = {styles.centerImage}
                            />
                        </View>
                        <TouchableOpacity onPress = {this.onRightPress.bind(this)} style = {styles.rightImageContainer}>
                            <Image
                                source = {{uri: this.getImageUrl(this.props.index + 1) }}
                                style = {styles.rightImage}
                            />
                        </TouchableOpacity>
                    </View>
                );
            }
        }
    }
}