import React from "react";
import { Component } from "react";
import {
    View,
    Text,
    TouchableOpacity,
} from "react-native";
import { styles } from "./styles";

export class TimeElement extends Component<any, any> {

    onItemPress(): void {
        this.props.onItemPress(this.props.time);
    }

    getStyle(): any {
        if (!this.props.free) {
            return styles.nonFree;
        }
        if (this.props.free && this.props.active) {
            return styles.active;
        } else {
            return styles.free;
        }
    }

    render(): JSX.Element {
        return (
            <TouchableOpacity style = {styles.padding} onPress = {this.onItemPress.bind(this)}>
                <View style = {this.getStyle.bind(this)()}>
                    <Text style = {styles.time}>
                        {this.props.time.replace(":00", "")}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}