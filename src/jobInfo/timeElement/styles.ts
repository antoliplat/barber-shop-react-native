import {Dimensions, StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    free: {
        width: Dimensions.get("screen").width * 0.25 - 16, //чтобы на экран помещалось строго 4 элемента, -16 для отступов
        height: Dimensions.get("screen").width * 0.125 - 16, //половина ширины
        borderWidth: 1,
        borderColor: "#464646",
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
    },
    nonFree: {
        width: Dimensions.get("screen").width * 0.25 - 16,
        height: Dimensions.get("screen").width * 0.125 - 16,
        alignItems: "center",
        justifyContent: "center",
    },
    active: {
        width: Dimensions.get("screen").width * 0.25 - 16,
        height: Dimensions.get("screen").width * 0.125 - 16,
        borderRadius: 4,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "#E5C999",
    },
    time: {
        fontSize: 14,
        color: "#464646",
    },
    padding: {
        paddingVertical: 8,
    },
});