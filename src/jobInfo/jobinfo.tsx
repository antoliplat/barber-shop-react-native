import React from "react";
import {Component} from "react";
import {
    View,
    Image,
    Text,
    TouchableOpacity,
    Button,
    ScrollView,
} from "react-native";
import { withNavigation } from "react-navigation";
import { styles } from "./styles";
import { constants } from "../constants/text";
import { DayElement } from "./dayElement/dayElement";
import { TimeElement } from "./timeElement/timeElement";
import { connect } from "react-redux";
import { ImageChanger } from "./imageChanger/imageChanger";
import {changeActiveDay, changeActiveTime, changeIndex, ScheduleRequest} from "../actions/jobInfo";

export class Job extends Component<any, any> {

    componentWillMount(): void {
        this.props.onTimePress("");
        this.props.onDayPress(0);
        const index = this.props.navigation.getParam("jobId");
        this.props.chngIndex(index);
        const id = this.props.services[index].id;
        const date = this.getTime(0);
        this.props.getSchedule({serviceId: id, date: date.toISOString()});
    }

    onBackPress(): void {
        this.props.navigation.pop();
    }

    onSubmitPress(): void {
        const url = "http://176.31.32.73:8001/api/Order/ToOrder";
        const now = new Date(Date.now());
        const hours = Number(this.props.activeTime.split(":")[0]);
        const minuts = Number(this.props.activeTime.split(":")[1]);
        const seanceTime = new Date(now.getFullYear(), now.getMonth(), now.getDate() + this.props.activeDay, hours - now.getTimezoneOffset() / 60, minuts);
        const id = this.props.services[this.props.index].id;
        const request = {
            body: JSON.stringify({serviceId: id, date: seanceTime.toISOString()}),
            headers: {
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
        };
        fetch(url, request)
            .then((response) => {
                if (response.ok) {
                    this.props.navigation.pop();
                }
            });
    }

    onJobPress(newIndex: number): void {
        this.props.chngIndex(newIndex);
        const id = this.props.services[newIndex].id;
        const date = this.getTime(0);
        this.props.getSchedule({serviceId: id, date: date.toISOString()});
        this.props.onDayPress(0);
        this.props.onTimePress("");
    }

    onDayElementPress(newDay: number): void {
        this.props.onDayPress(newDay);
        const id = this.props.services[this.props.index].id;
        const date = this.getTime(newDay);
        this.props.getSchedule({serviceId: id, date: date.toISOString()});
        this.props.onTimePress("");
    }

    getTimeElement(element: any): JSX.Element {
        return(
            <TimeElement
                time = {element.value}
                free = {element.free}
                active = {element.value === this.props.activeTime}
                onItemPress = {this.onTimeElementPress.bind(this)}
            />
        );
    }

    onTimeElementPress(time: string): void {
        this.props.onTimePress(time);
    }

    getTime(newDay: number): Date {
        const now = new Date(Date.now());

        return new Date(now.getFullYear(), now.getMonth(), now.getDate() + newDay, -now.getTimezoneOffset() / 60);
    }

    getDuration(): string {
        return this.props.services[this.props.index].duration.replace("00:", "").replace(":00", "") + constants.MINUTES + "/ " + this.props.services[this.props.index].price + " " + constants.MONEY;
    }

    getDayElement(item: any, position: number): JSX.Element {
        return (
            <DayElement
                pos = {position}
                active = {this.props.activeDay === position}
                onDayPress = {this.onDayElementPress.bind(this)}
            />
        );
    }

    public render(): JSX.Element {
        return(
            <View style = {styles.container}>
                <View style = {styles.topBar}>
                    <TouchableOpacity onPress = {this.onBackPress.bind(this)}>
                        <Image source = {{uri: "icon_back"}} style = {styles.iconBack} />
                    </TouchableOpacity>
                    <Text style = {styles.month}>
                        {constants.MONTH[this.getTime(this.props.activeDay).getMonth()]}
                    </Text>
                </View>
                <View style = {styles.days}>
                    {constants.NUMBER_OF_DAYS.map(this.getDayElement.bind(this))}
                </View>
                <ImageChanger
                    onImagePress = {this.onJobPress.bind(this)}
                    index = {this.props.index}
                    services = {this.props.services}
                />
                <Text style = {styles.name}>
                    {this.props.services[this.props.index].name}
                </Text>
                <Text style = {styles.duration}>
                    {this.getDuration.bind(this)()}
                </Text>
                <ScrollView style = {styles.scroll}>
                    <View style = {styles.time}>
                        {this.props.schedule.map(this.getTimeElement.bind(this))}
                    </View>
                </ScrollView>
                <View style = {styles.bottomContainer}>
                    <View style = {styles.button}>
                        <Button
                            onPress = {this.onSubmitPress.bind(this)}
                            title = {constants.SUBMIT}
                            color = "#464646"
                            disabled = {this.props.activeTime === ""}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const JobWithNav = withNavigation(Job);

const mapStateToProps = (state: any): any => {
    return {
        services: state.services,
        schedule: state.schedule,
        activeDay: state.activeDay,
        activeTime: state.activeTime,
        index: state.index,
    };
};

const mapDispatchToProps = (dispatch: any): any => {
    return {
        chngIndex: (index: number): void => {
            dispatch(changeIndex(index));
        },
        getSchedule: (request: any): void => {
            dispatch(ScheduleRequest.action(request));
        },
        onTimePress: (time: string): void => {
            dispatch(changeActiveTime(time));
        },
        onDayPress: (day: number): void => {
            dispatch(changeActiveDay(day));
        }
    };
};

export const JobInfo = connect(mapStateToProps, mapDispatchToProps)(JobWithNav);