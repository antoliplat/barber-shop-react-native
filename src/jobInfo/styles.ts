import {Platform, StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    topBar: {
        backgroundColor: "#464646",
        flexDirection: "row",
        paddingRight: 50,
    },
    iconBack: {
        width: 50,
        height: 50,
        resizeMode: "center",
    },
    month: {
        fontSize: 17,
        color: "#E5C999",
        fontWeight: "bold",
        marginTop: 15,
        textAlign: "center",
        marginLeft: "auto",
        marginRight: "auto",
    },
    days: {
        flexDirection: "row",
        backgroundColor: "#464646",
        height: "10%",
        justifyContent: "space-around",
    },
    container: {
        flex: 1,
    },
    dayOfMonth: {
        color: "#FFFFFF",
        fontSize: 18,
        textAlign: "center",
    },
    dayOfWeek: {
        color: "#FFFFFF",
        fontSize: 14,
        textAlign: "center",
    },
    active: {
        borderRadius: 75,
        backgroundColor: "#E5C999",
        width: 50,
        height: 50,
    },
    nothing: {
        width: 50,
        height: 50,
    },
    button: {
        width: 183,
    },
    bottomContainer: {
        alignItems: "center",
        paddingBottom: 16,
    },
    time: {
        flex: 1,
        flexWrap: "wrap",
        flexDirection: "row",
        justifyContent: "space-around",
    },
    name: {
        fontFamily: "KellySlab-Regular",
        fontSize: 22,
        color: "#464646",
        textAlign: "center",
    },
    duration: {
        fontSize: 16,
        color: "#87888C",
        textAlign: "center",
        fontFamily: Platform.OS === "ios" ? "SFUIText-Regular" : "",
    },
    scroll: {
        flex: 1,
    }
});