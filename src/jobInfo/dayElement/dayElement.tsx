import React from "react";
import { Component } from "react";
import {
    View, Text, TouchableOpacity,
} from "react-native";
import { styles } from "./styles";
import { constants } from "../../constants/text";

export class DayElement extends Component<any, any> {

    getDay(delta: number): number {
        const now = new Date(Date.now());
        const date = new Date(now.getFullYear(), now.getMonth(), now.getDate() + delta);

        return date.getDay();
    }

    getDate(delta: number): number {
        const now = new Date(Date.now());
        const date = new Date(now.getFullYear(), now.getMonth(), now.getDate() + delta);

        return date.getDate();
    }

    onDayPress(): void {
        this.props.onDayPress(this.props.pos);
    }

    render(): JSX.Element {
        return (
            <TouchableOpacity onPress = {this.onDayPress.bind(this)}>
                <View style = {this.props.active ? styles.active : styles.nothing}>
                    <Text style = {this.props.active ? styles.dayOfMonthActive : styles.dayOfMonth}>
                        {this.getDate(this.props.pos)}
                    </Text>
                    <Text style = {this.props.active ? styles.dayOfWeekActive : styles.dayOfWeek}>
                        {constants.WEEK[this.getDay(this.props.pos)]}
                    </Text>
                </View>
            </TouchableOpacity>
        );
    }
}