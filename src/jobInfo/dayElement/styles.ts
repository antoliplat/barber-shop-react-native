import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    dayOfMonth: {
        color: "#FFFFFF",
        fontSize: 18,
        textAlign: "center",
    },
    dayOfWeek: {
        color: "#FFFFFF",
        fontSize: 14,
        textAlign: "center",
    },
    active: {
        borderRadius: 75,
        backgroundColor: "#E5C999",
        width: 50,
        height: 50,
    },
    nothing: {
        width: 50,
        height: 50,
    },
    dayOfMonthActive: {
        color: "#464646",
        fontSize: 18,
        textAlign: "center",
    },
    dayOfWeekActive: {
        color: "#464646",
        fontSize: 14,
        textAlign: "center",
    }
});